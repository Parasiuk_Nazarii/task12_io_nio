package view;

import controller.CommentController;
import controller.DirectoryController;
import controller.FileController;
import controller.IO_Controller;
import model.NIO_ClientServer.NioClient;
import model.NIO_ClientServer.Server;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Write to file and read from it about the space ship");
        menu.put("2", "  2 - Choose java file to read all comments");
        menu.put("3", "  3 - Read all files from directory");
        menu.put("4", "  4 - Start NIO Server and Client ");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        IO_Controller ioc = new IO_Controller();
        ioc.writeToFile();
        ioc.readFromFile();
    }

    private void pressButton2() {
        CommentController cc = new CommentController();
        cc.readAllComments();
    }

    private void pressButton3() {
        DirectoryController dc = new DirectoryController();
        File file = new FileController().chooseDirectory();
        dc.printDirectory(file);
        dc.activateCommands(file);
    }

    private void pressButton4() {
        Server.main();
        NioClient.main();
    }

    private void pressButton5() {


    }

    private void pressButton6() {

    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
