package model;

import java.io.Serializable;
import java.util.Map;

public class Ship implements Serializable {
    private String name;
    private Map<Droid, Integer> droids;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Droid, Integer> getDroids() {
        return droids;
    }

    public void setDroids(Map<Droid, Integer> droids) {
        this.droids = droids;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Ship(String name, Map<Droid, Integer> droids, String description) {
        this.name = name;
        this.droids = droids;
        this.description = description;
    }

    public Ship() {
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", droids=" + droids +
                ", description='" + description + '\'' +
                '}';
    }
}
