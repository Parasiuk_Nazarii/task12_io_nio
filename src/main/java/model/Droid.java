package model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String id;
    private int speed;
    private boolean weapon;
    private transient double price;
    private transient String description;

    public Droid(String id, int speed, boolean weapon, double price, String description) {
        this.id = id;
        this.speed = speed;
        this.weapon = weapon;
        this.price = price;
        this.description = description;
    }

    public Droid() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public boolean isWeapon() {
        return weapon;
    }

    public void setWeapon(boolean weapon) {
        this.weapon = weapon;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Droid)) return false;

        Droid droid = (Droid) o;

        return getId().equals(droid.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        return "Droid{" +
                "id='" + id + '\'' +
                ", speed=" + speed +
                ", weapon=" + weapon +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
