package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CommentController {
    public void readAllComments() {
        try {
            File file = new FileController().chooseFile("JAVA FILES", "java");
            BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("/**")) {
                    System.out.println(line);
                    while (!line.contains("*/")) {
                        System.out.println(line = br.readLine());
                    }
                }
                if (line.contains("//")) {
                    System.out.println(line);
                }

            }
            br.close();
        } catch (IOException e) {
            System.out.println("OOPS! File could not read!");
        }

    }
}
