package controller;

import model.Droid;
import model.Ship;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class IO_Controller {
    public Ship initShip() {
        Droid droid1 = new Droid("id001", 500, true, 2000, "light fight droid");
        Droid droid2 = new Droid("id002", 700, true, 4500, "very fast scout droid");
        Droid droid3 = new Droid("id003", 300, false, 4000, "cargo droid without weapon");

        Map<Droid, Integer> droids = new HashMap<Droid, Integer>();
        droids.put(droid1, 15);
        droids.put(droid2, 4);
        droids.put(droid3, 2);

        Ship ship = new Ship();
        ship.setName("BFR");
        ship.setDroids(droids);
        ship.setDescription("First star ship created by Elon Mask");
        return ship;
    }

    public void writeToFile() {
        try {
            FileOutputStream file = new FileOutputStream("src/txt/ship.txt");
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(initShip());
            out.close();
            file.close();
            System.out.println("Success");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFromFile() {
        Ship ship = null;
        try {
            FileInputStream fis = new FileInputStream("src/txt/ship.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ship = (Ship) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(ship);
    }

}
